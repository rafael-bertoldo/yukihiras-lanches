import "./style.css";

function ProductCar({ currentSale }) {
  return (
    <div className="main-sale">
      <div className="item-description">
        <h2>{currentSale.category}</h2>
        <h2>{currentSale.name}</h2>
        <h3>R$ {currentSale.price}</h3>
      </div>
    </div>
  );
}

export default ProductCar;
