import { useState, useEffect } from "react";
import { BsSearch } from "react-icons/bs";
import "./App.css";
import MenuContainer from "./components/MenuContainer";
import ProductCar from "./components/ProductCart";

function App() {
  const [products, setProducts] = useState([
    { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
    { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
    { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
    { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
    { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
    { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
    { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
  ]);
  const reset = [
    { id: 1, name: "Hamburguer", category: "Sanduíches", price: 7.99 },
    { id: 2, name: "X-Burguer", category: "Sanduíches", price: 8.99 },
    { id: 3, name: "X-Salada", category: "Sanduíches", price: 10.99 },
    { id: 4, name: "Big Kenzie", category: "Sanduíches", price: 16.99 },
    { id: 5, name: "Guaraná", category: "Bebidas", price: 4.99 },
    { id: 6, name: "Coca", category: "Bebidas", price: 4.99 },
    { id: 7, name: "Fanta", category: "Bebidas", price: 4.99 },
  ];
  const [input, setInput] = useState("");
  const [showVoltar, setShowVoltar] = useState(false);

  function showProducts() {
    setProducts(
      products.filter((item) => {
        return item.name.toUpperCase() === input.toUpperCase();
      })
    );
    setShowVoltar(true);
    setInput("");
  }

  const [currentSale, setCurrentSale] = useState([]);
  const [cartTotal, setCartTotal] = useState(0);

  function handleClick(numb) {
    const newSaleItem = products.find((item) => item.id === numb);
    if (currentSale.includes(newSaleItem)) {
      alert("produto já adicionado ao carrinho");
    } else {
      setCurrentSale([...currentSale, newSaleItem]);
    }
  }

  function handleReset() {
    setProducts(reset);
    setShowVoltar(false);
  }

  useEffect(() => {
    setCartTotal(
      currentSale.reduce((valorAnterior, ValorAtual) => {
        return ValorAtual.price + valorAnterior;
      }, 0)
    );
  }, [currentSale]);

  function handleclearCurrentSale() {
    setCurrentSale([]);
  }

  return (
    <div className="App">
      <h1 className="hamburgueria-title">Yukihira's Lanches</h1>
      <div className="search-container">
        <input
          className="input"
          placeholder="pesquisar produto"
          type="text"
          value={input}
          onChange={(e) => setInput(e.target.value)}
        />
        <button id="btn" onClick={showProducts}>
          <BsSearch />
        </button>
      </div>
      <div className="btn-container">
        {!showVoltar ? (
          <div></div>
        ) : (
          <button className="btn" onClick={handleReset}>
            voltar
          </button>
        )}
      </div>
      <MenuContainer products={products} handleClick={handleClick} />

      <h3 className="sale-title">Seus Pedidos:</h3>
      <div className="sale">
        {currentSale.map((item, index) => (
          <ProductCar key={index} currentSale={item} />
        ))}
      </div>
      <span className="sale-value">Valor Total: R$ {cartTotal.toFixed(2)}</span>
      <button className="btn" onClick={handleclearCurrentSale}>
        limpar Carrinho
      </button>
    </div>
  );
}

export default App;
