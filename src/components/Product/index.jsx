import "./style.css";
import { CgShoppingBag } from "react-icons/cg";

function Product({ handleClick, item }) {
  return (
    <div className="item">
      <div className="item-description">
        <h2>{item.category}</h2>
        <h2>{item.name}</h2>
        <h3>R$ {item.price}</h3>
      </div>
      <button className="btn" onClick={() => handleClick(item.id)}>
        incluir pedido <CgShoppingBag size={28} />
      </button>
    </div>
  );
}

export default Product;
