import Product from "../Product";
import "./style.css";

function MenuContainer({ products, handleClick }) {
  return (
    <div className="main">
      {products.map((item) => (
        <Product key={item.id} item={item} handleClick={handleClick} />
      ))}
    </div>
  );
}

export default MenuContainer;
